CFLAGS=-Wall
LIBS= -lpthread
OBJS = main.c parametry.c watki.c watcher.c log.c client.c rstart.c
all: synchro

synchro: $(OBJS)
	$(CC) $(CFLAGS) $(OBJS) -o synchro $(LIBS)

main.o: main.c
	$(CC) -c $(CFLAGS) main.c -o main.o

parametry.o: parametry.c
	$(CC) -c $(CFLAGS) parametry.c -o parametry.o


watki.o: watki.c
	$(CC) -c $(CFLAGS) watki.c -o watki.o $(LIBS)

watcher.o: watcher.c
	$(CC) -c  $(CFLAGS) watcher.c -o watcher.o

client.o: client.c
	$(CC) -c  $(CFLAGS) client.c -o client.o

log.o: log.c
	$(CC) -c  $(CFLAGS) log.c -o log.o
rstart.o: rstart.c
	$(CC) -c  $(CFLAGS) rstart.c -o rstart.o
clean:
	rm *.o synchro -f

