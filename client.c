#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <string.h>

#include "parametry.h"
#include "log.h"

int client(char* wiadomosc){

	int sockfd;
	struct sockaddr_in servaddr;
	struct hostent *server;
	char msg[256];
	int yes = 1;
	int ret;

	bzero((char *) &servaddr, sizeof(servaddr));
	server = gethostbyname(parametry.hostname);
	if(server == NULL){
		logString("Nieprawidłowy hostname\n");
		exit(1);
	}
		
	servaddr.sin_family = AF_INET;
	servaddr.sin_port = htons(parametry.portc);
	    bcopy((char *)server->h_addr, 
         (char *)&servaddr.sin_addr.s_addr,
         server->h_length);


	sockfd = socket(AF_INET, SOCK_STREAM, 0);

	

	setsockopt(sockfd,SOL_SOCKET,SO_REUSEADDR,&yes,sizeof(int));
    	bzero(msg,256);
    	strcpy(msg,wiadomosc);
	if(connect(sockfd, (struct sockaddr*)&servaddr, sizeof(servaddr)) == 0){
		write(sockfd, msg, strlen(msg));
		read(sockfd, msg, 255);
		logString("Odpowiedz serwera: %s\n",msg);
		ret = atoi(msg);
	}
	else{
		logString("no serwer connection.\n");
		ret = 0;
	}
	

	close(sockfd);
return ret;
}
