#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>

#include "parametry.h"


void logString(const char * message, ...){
	
	//printf("%d \n", parametry.daemon);
	if(parametry.daemon){

		FILE * logfile;
		logfile = fopen("Log.txt", "a");
		va_list args;
		va_start (args, message);
		vfprintf (logfile, message, args);
		va_end (args);
		fclose(logfile);
	}
	else{
		va_list args;
		va_start (args, message);
		vprintf(message, args);
		va_end(args);
	}
}
void logStringErr(const char * message, ...){
	
	//printf("%d \n", parametry.daemon);
	if(parametry.daemon){

		FILE * logfile;
		logfile = fopen("Log.txt", "a");
		va_list args;
		va_start (args, message);
		vfprintf (logfile, message, args);
		va_end (args);
		fclose(logfile);
		exit(0);
	}
	else{
		va_list args;
		va_start (args, message);
		vprintf(message, args);
		va_end(args);
		exit(0);
	}
}
