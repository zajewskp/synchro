#ifndef LOG_H
#define LOG_H

#include <stdio.h>
#include <stdlib.h>

void logString (const char *, ...);
void logStringErr (const char *, ...);

#endif
