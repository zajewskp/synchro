#include <stdio.h>
#include <stdlib.h>

#include "parametry.h"
#include "watki.h"
#include "client.h"
#include "log.h"

void startDaemon();

int main (int argc, char **argv){

	sprawdzaj_parametry(argc, argv);
	if(parametry.daemon)
		startDaemon();
	else
		watki();

return 0;
}

void startDaemon(){
		pid_t pid = 0;
		pid_t sid = 0;
		// rozgalezianie procesu
		pid = fork();
		// blad
		if (pid == -1){
			logString("fork nie utworzony!\n");
			exit(1);
		}
		// rodzic
		if (pid > 0){
			logString("proces id %d \n", pid);
			exit(0);
		}
		//nowa sesja daemon
		sid = setsid();
		if(sid < 0)
			exit(1);
		close(STDIN_FILENO);
		close(STDOUT_FILENO);
		close(STDERR_FILENO);
		watki();
}
