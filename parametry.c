#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <stdbool.h>

#include "parametry.h"
#include "log.h"


struct parametry_t parametry;

void inpeer1();
void inpeer2();
void start();

void sprawdzaj_parametry (int argc, char **argv){
	


	/*wypelnij strukture wartosciami domyslnymi*/
	parametry.pomoc = false;
	parametry.wersja = false;
	parametry.daemon = false;
	parametry.peer1 = false;
	parametry.peer2 = false;
	parametry.update = false;
	parametry.adres = NULL;
	parametry.hostname = NULL;	
	parametry.plik = "conf";
	int opt;
	int longind = 1;

	static struct option dlugie_opcje[] = {
		{"help",0,0,'h'},
		{"version",0,0,'v'},
		{"daemon",0,0,'d'},
		{"peer1",0,0,'1'},
		{"peer2",0,0,'2'},
		{"file",1,0,'f'},
		{"update",0,0,'u'},
	};

		while ((opt = getopt_long(argc, argv, "hvd12uf:", dlugie_opcje, &longind)) != -1) {
			switch (opt) {
				case 'h':
					parametry.pomoc = true;
					break;
				case 'v':
					parametry.wersja = true;
					break;
				case 'd':
					parametry.daemon = true;
					break;
				case '1':
					parametry.peer1 = true;
					parametry.ports = 28000;
					parametry.portc = 28001;
					break;
				case '2':
					parametry.peer2 = true;
					parametry.ports = 28001;
					parametry.portc = 28000;
					break;
				case 'f':
					parametry.plik = malloc(strlen(optarg));
					strcpy(parametry.plik,optarg);
					break;
				case 'u':
					parametry.update = true;
					break;
				default: /* '?' */
					logStringErr("Nieprawidłowy parametr. Aby dowiedzieć się więcej na temat wywołania programu wpisz ./synchro -h\n");
			}
		}




		logString("help %d\n", parametry.pomoc);
		logString("version %d\n", parametry.wersja);	
		logString("daemon %d\n", parametry.daemon);
		logString("port serwera %d\n", parametry.ports);
		logString("port klienta %d\n", parametry.portc);	
		logString("plik konfiguracyjny: %s\n", parametry.plik);



	if(parametry.pomoc){

		printf("##################################################################\n");
		printf("#                           Pomoc                                #\n");
		printf("# Program do synchronizacji plików po sieci lan                  #\n");
		printf("# Aby zsynchronizować foldery, należy uruchomić program na       #\n");
		printf("# Dwóch maszynach z parametrem -s na jednej i parametrem -c      #\n");
		printf("# na drugiej. Dodatkowo każdy z programów musi podać adres       #\n");
		printf("# drugiego urządzenia poprzedzony parametrem -t                  #\n");
		printf("#                                                                #\n");
		printf("#                                                                #\n");
		printf("#                                                                #\n");
		printf("# Dostepne parametry:                                            #\n");
		printf("#                                                                #\n");
		printf("#  -h --help    pokazuje ten ekran pomocy                        #\n");
		printf("#                                                                #\n");
		printf("#  -v --version pokazuje wersje programu i autora                #\n");
		printf("#                                                                #\n");
		printf("#  -1 --peer1  program z tym parametrem łączy się z...           #\n");
		printf("#                                                                #\n");
		printf("#  -2 --peer2  ...programem z tym parametrem                     #\n");
		printf("#                                                                #\n");
		printf("#  -t --to      podanie adresu drugiego urządzenia user@hostname #\n");
		printf("#                                                                #\n");
		printf("#  -f --file    plik konfiguracyjny w którym zapisane są ścieżki #\n");
		printf("#               do folderów które będą podlegać synchronizacji.  #\n");
		printf("#               Foldery te powinny być zapisywane naprzemiennie  #\n");
		printf("#               Domyślny plik konfiguracyjny to .config          #\n");
		printf("#                                                                #\n");
		printf("#  -u --update  tworzenie listy plików w obserwowanych folderach #\n");
		printf("#               po użyciu tej opcji historia usuniętych plików   #\n");
		printf("#               od czasu ostatniego uruchomienia programu        #\n");
		printf("#               zostanie skasowana                               #\n");
		printf("#                                                                #\n");
		printf("#  -d --daemon  wykonywanie programu w trybie daemon. Logi       #\n");
		printf("#               zapisywane zostają w pliku Log.txt               #\n");
		printf("#                                                                #\n");
		printf("#                                                                #\n");
		printf("#                                                                #\n");
		printf("# Aby program działał poprawnie maszyny powinny być połączone    #\n");
		printf("# ze sobą przez protokół SSH, i mieć wymienione ze sobą klucze   #\n");
		printf("##################################################################\n");   

	}
	if(parametry.wersja){

		printf("############################################################\n");
		printf("#                        Synchro V 0.3                     #\n");
		printf("#                                                          #\n");    
		printf("#          Autor: Paweł Jan Zajewski  (nr.228076)          #\n");
		printf("#       Program do synchronizacji folderów po sieci lan    #\n");
		printf("############################################################\n");

	}

	if(parametry.pomoc == 1 || parametry.wersja == 1)
		exit(EXIT_SUCCESS);	

	if(parametry.plik == NULL)
		logStringErr("brak pliku konfiguracyjnego! Aby dowiedzieć się więcej wpisz ./synchro -h\n");

	if(parametry.peer1 == true)
		inpeer1();
	if(parametry.peer2 == true)
		inpeer2();
	if(parametry.adres == NULL)
		logStringErr("brak adresu drugiego urządzenia! Aby dowiedzieć się więcej na temat wywołania programu wpisz ./synchro -h \n");	
	if(parametry.update == true){
		start();
	}
	logString("path to %s\n", parametry.adres);
	logString("hostname %s\n", parametry.hostname);	
}

void start(){
	int i = 0;
	char cmdLine[4096];
	sprintf(cmdLine, "rm .lastsync");
	system(cmdLine);
	logString("tworzenie listy plików .lastsync w obserwowanych folderach.. \n");
	while(parametry.lista[i].sod != NULL){
		char cmdLine[4096];
		sprintf(cmdLine, "find %s >> .lastsync", parametry.lista[i].sod);
		system(cmdLine);
		i++;
	}
	logString("lista plików .lastsync dodana \n");
}

void inpeer1(){

		FILE *file = fopen(parametry.plik, "r");
		int counter = 0;
		int i = 0;
		int j = 0;
		size_t size = 50;
		char line [1024];

		if((parametry.lista = malloc(size * sizeof(foldery_t))) == NULL)
			logString("nie zaalokowano pamięci dla lista");

		if (file != NULL){
			while((fgets( line, sizeof line, file )) != NULL){
				int len = strlen(line);
				if (len > 0 && line[len-1] == '\n'){
					line[len-1] = '\0';
					len--;
				}
				if ( i == size ) {
					size *= 2;
					if(!(parametry.lista = realloc(parametry.lista, size * sizeof(foldery_t))))
						logStringErr("Nie udalo sie realokować lista");
					

				}
				if(counter == 1){
					parametry.adres = malloc(sizeof(line));
					strcpy(parametry.adres,line);
					parametry.hostname = malloc(sizeof(line));
					while(j < strlen(parametry.adres)){
						if(parametry.adres[j] == '@'){
						j++;
						strcpy(parametry.hostname, parametry.adres + j);
						break;
						}
						j++;
					}
					if(j == strlen(parametry.adres)){
						strcpy(parametry.hostname, parametry.adres);
					}
				}
				if(counter > 1 && counter % 2 == 0){
					if(line[len-1] != '/'){
    						line[len] = '/';
    						line[len + 1] = '\0';
					}
					parametry.lista[i].sod = malloc(sizeof(line));
					strcpy(parametry.lista[i].sod, line);
					logString("%dwiersz od: %s\n", i, parametry.lista[i].sod);

				}
				else if(counter > 1 && counter % 2 != 0){
					if(line[len-1] == '/')
						line[len-1] = '\0';
					parametry.lista[i].sdo = malloc(sizeof(line));
					strcpy(parametry.lista[i].sdo, line);
					logString("%dwiersz do: %s\n", i, parametry.lista[i].sdo);
					i++;
				}
  				counter++;
			}
			if(counter == 0 || counter % 2 != 0)
				logStringErr("zły format pliku\n");

		}
		else
			logStringErr("błąd wczytywania pliku\n");		
}
void inpeer2(){

		FILE *file = fopen(parametry.plik, "r");
		int counter = 0;
		int i = 0;
		int j = 0;
		size_t size = 50;
		char line [1024];

		if((parametry.lista = malloc(size * sizeof(foldery_t))) == NULL)
			logString("nie zaalokowano pamięci dla lista");

		if (file != NULL){
			while((fgets( line, sizeof line, file )) != NULL){
				int len = strlen(line);
				if (len > 0 && line[len-1] == '\n'){
					line[len-1] = '\0';
					len--;
				}
				if ( i == size ) {
					size *= 2;
					if(!(parametry.lista = realloc(parametry.lista, size * sizeof(foldery_t))))
						logStringErr("Nie udalo sie realokować lista");

				}
				if(counter == 0){
					parametry.adres = malloc(strlen(line));
					strcpy(parametry.adres,line);
					parametry.hostname = malloc(strlen(line));
					while(j < strlen(parametry.adres)){
						if(parametry.adres[j] == '@'){
						j++;
						strcpy(parametry.hostname, parametry.adres + j);
						break;
						}
						j++;
					}
					if(j == strlen(parametry.adres)){
						strcpy(parametry.hostname, parametry.adres);
					}
				}

				if(counter > 1 && counter % 2 == 0){
					if(line[len-1] == '/')
						line[len-1] = '\0';
					parametry.lista[i].sdo = malloc(sizeof(line));
					strcpy(parametry.lista[i].sdo, line);
					logString("%dwiersz do: %s\n", i, parametry.lista[i].sdo);
				}
				else if(counter > 1 && counter % 2 != 0){
					if(line[len-1] != '/'){
    						line[len] = '/';
    						line[len + 1] = '\0';
					}
					parametry.lista[i].sod = malloc(sizeof(line));
					strcpy(parametry.lista[i].sod, line);
					logString("%dwiersz od: %s\n", i, parametry.lista[i].sod);
					i++;
				}
  				counter++;
			}
			if(counter == 0 || counter % 2 != 0)
				logStringErr("zły format pliku\n");

		}
		else
			logStringErr("błąd wczytywania pliku\n");	
}
