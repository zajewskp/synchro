#ifndef PARAMETRY_H
#define PARAMETRY_H

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>

typedef struct{
	char* sod;
	char* sdo;
} foldery_t;

struct parametry_t {

	bool pomoc;
	bool wersja;
	bool daemon;
	bool peer1;
	bool peer2;
	bool update;
	int ports;
	int portc;
	char *adres;
	char *hostname;
	char *plik;
	foldery_t * lista;
	char** del;
};
extern struct parametry_t parametry;

void sprawdzaj_parametry(int,char**); /*funkcja która wypełnia strukturę i sprawdza poprawność argumentów*/
void start();

#endif
