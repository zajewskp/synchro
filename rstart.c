#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "parametry.h"
#include "log.h"

bool file_exists(const char * filename);
void diff();
char *zamien(char *st, char *orig, char *repl);

void rstart(){
	int i = 0;
	char cmdLine[4096];

	if(!file_exists(".lastsync")){
		sprintf(cmdLine, "touch .lastsync");
		system(cmdLine);		
	}
		
	sprintf(cmdLine, "rm .newsync");
	system(cmdLine);
	logString("tworzenie listy plików w obserwowanych folderach.. \n");
	while(parametry.lista[i].sod != NULL){
		logString("find:: %s \n", parametry.lista[i].sod);
		char cmdLine[4096];
		sprintf(cmdLine, "find %s >> .newsync", parametry.lista[i].sod);
		system(cmdLine);
		i++;
	}
	sprintf(cmdLine, "rm diff");
	system(cmdLine);
	logString("lista plików .newsync dodana \n");
	sprintf(cmdLine, "diff .newsync .lastsync >> diff");
	system(cmdLine);
	diff();
}

void diff(){
		FILE *file = fopen("diff", "r");
		int i = 0;
		size_t size = 50;
		char line [1024];
		if((parametry.del = (char**)malloc(size * sizeof(char))) == NULL)
			logString("nie zaalokowano pamięci dla parametry.del");
		if (file != NULL){
			while((fgets( line, sizeof line, file )) != NULL){
				int len = strlen(line);
				if (len > 0 && line[len-1] == '\n'){
					line[len-1] = '\0';
					len--;
				}
				if ( i == size ) {
					size *= 2;
					if(!(parametry.del = (char**)realloc(parametry.del, size * sizeof(char)))){
						logString("Nie udalo sie realokować lista"); exit(1);
					}

				}
				if( line[0] == '>'){
				//	if(line[len-1] != '/'){
    				//		line[len] = '/';
    				//		line[len + 1] = '\0';
				//	}
    					parametry.del[i] = (char*)malloc(1024 * sizeof(char));
					if(i == 0){
						strcpy(parametry.del[i],line+2);
						logString("do usuniecia: %s\n", parametry.del[i]);
						i++;
					}
					else if(strncmp(parametry.del[i-1], line+2, strlen(parametry.del[i-1]) * sizeof(char)) != 0){
						strcpy(parametry.del[i],line+2);
						logString("do usuniecia: %s\n", parametry.del[i]);
						i++;
					}
				}
			}
    			parametry.del[i] = (char*)malloc(1024 * sizeof(char));
			parametry.del[i] = NULL;			


		}
		else{
			logString("błąd wczytywania pliku\n");
			exit(1);
		}
		
}

void rrm(){

	int i = 0;
	int j = 0;
	char *del;
	while(parametry.del[i] != NULL){
		del = zamien(parametry.del[i], parametry.lista[j].sod, parametry.lista[j].sdo);
		if(del == parametry.del[i]){
			j++;
			del = zamien(parametry.del[i], parametry.lista[j].sod, parametry.lista[j].sdo);
		}
		logString("zamiana sod na sdo: %s\n", del);	
		char cmdLine[4096];
		sprintf(cmdLine, "ssh %s rm -rf %s",parametry.adres, del);
		logString("ssh %s rm -rf %s",parametry.adres, del);
		system(cmdLine);		
		i++;
	}

}
		
void rsync(){

	int k = 0;
	char cmdLine[4096];

	while(parametry.lista[k].sod != NULL){
	sprintf(cmdLine, "rsync -Cavuzb %s %s:%s", parametry.lista[k].sod, parametry.adres, parametry.lista[k].sdo);
	system(cmdLine);
	k++;
	}	
}

char *zamien(char *st, char *orig, char *repl) {
  static char buffer[4096];
  char *ch;
  if (!(ch = strstr(st, orig)))
   return st;
  strncpy(buffer, st, ch-st);  
  buffer[ch-st] = 0;
  sprintf(buffer+(ch-st), "%s%s", repl, ch+strlen(orig));
  return buffer;
  }

bool file_exists(const char * filename)
{
    FILE * file;
    if ((file = fopen(filename, "r")))
    {
        fclose(file);
        return true;
    }
    return false;
}
