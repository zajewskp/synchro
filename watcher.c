#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/inotify.h>
#include <dirent.h>
#include <limits.h>
#include <string.h>

#include "log.h"
#include "parametry.h"
#include "client.h"
#include "rstart.h"

#define MAX_LEN 1024 
#define EVENT_SIZE  ( sizeof (struct inotify_event) )
#define BUF_LEN     ( 1024 * ( EVENT_SIZE + 16 ) )

int size = 50;
int j = 0;
foldery_t * lista;


void add_watches(int, char *);

int watcher() {
	int length, i = 0, wd;
	int fd;
	char buffer[BUF_LEN];



	fd = inotify_init();
	if ( fd < 0 )
		logString( "Nie uruchomiono inotify\n");

	if((lista = malloc(size * sizeof(foldery_t))) == NULL)
		logStringErr("nie zaalokowano pamięci dla lista");
 
	while(parametry.lista[j].sod != NULL){
		add_watches(fd,parametry.lista[j].sod);
		j++;
	}

				logString("DODANO DO LISTA[0].SDO: %s\n",lista[0].sdo); //nie wiem dlaczego
				logString("DODANO DO LISTA[1].SDO: %s\n",lista[1].sdo);
				logString("DODANO DO LISTA[2].SDO: %s\n",lista[2].sdo);
	while(1){
		i = 0;
		length = read( fd, buffer, BUF_LEN ); 
 		char cmdLine[4096];
		if ( length < 0 )
			perror( "Nie odczytano fd\n" );
 
		while ( i < length ) {
			struct inotify_event *event = ( struct inotify_event * ) &buffer[ i ];
			if ( event->len ) {
				if ( event->mask & IN_CREATE) {
					if (event->mask & IN_ISDIR){
						logString( "Dodano folder %s \n", event->name );
						logString("rsync -Cavuzb %s%s %s:%s &\n", lista[event->wd].sod, event->name, parametry.adres, lista[event->wd].sdo);
						sprintf(cmdLine, "rsync -Cavuzb %s/%s %s:%s &", lista[event->wd].sod, event->name, parametry.adres, lista[event->wd].sdo);
						system(cmdLine);
					}      
					else{
						logString( "Dodano plik %s \n", event->name );
						logString("rsync -Cavuzb %s/%s %s:%s &\n", lista[event->wd].sod, event->name, parametry.adres, lista[event->wd].sdo);
						sprintf(cmdLine, "rsync -Cavuzb %s/%s %s:%s &", lista[event->wd].sod, event->name, parametry.adres, lista[event->wd].sdo);
						system(cmdLine);
					}         
				}
           
				if ( event->mask & IN_MODIFY) {
					if (event->mask & IN_ISDIR){
						logString( "Zmodyfikowano folder %s \n", event->name );
						logString("rsync -Cavuzb %s%s %s:%s &\n", lista[event->wd].sod, event->name, parametry.adres, lista[event->wd].sdo);
						sprintf(cmdLine, "rsync -Cavuzb %s/%s %s:%s &", lista[event->wd].sod, event->name, parametry.adres, lista[event->wd].sdo);
						system(cmdLine);
					}    
					else{
						logString( "Zmodyfikowano plik %s \n", event->name);
						logString("rsync -Cavuzb %s%s %s:%s &\n", lista[event->wd].sod, event->name, parametry.adres, lista[event->wd].sdo);
						sprintf(cmdLine, "rsync -Cavuzb %s/%s %s:%s &", lista[event->wd].sod, event->name, parametry.adres, lista[event->wd].sdo);
						system(cmdLine);
					}  
 
				}
				if ( event->mask & IN_DELETE) {
					if (event->mask & IN_ISDIR){
						logString( "Skasowano folder %s \n", event->name );  
						logString("ssh %s rm -rf %s/%s\n",parametry.adres, lista[event->wd].sdo, event->name);
						sprintf(cmdLine, "ssh %s rm -r %s/%s",parametry.adres, lista[event->wd].sdo, event->name);
						system(cmdLine);
					}    
					else{
						logString( "Skasowano plik %s w WD %d \n", event->name, event->wd);
						logString("ssh %s rm -rf %s/%s\n",parametry.adres, lista[event->wd].sdo, event->name);
						sprintf(cmdLine, "ssh %s rm -r %s/%s",parametry.adres, lista[event->wd].sdo, event->name);
						system(cmdLine);
					}      
   			       }        
			i += EVENT_SIZE + event->len;
			}
		}
	}

	inotify_rm_watch( fd, wd );
	close( fd );
   
return 0;
}


 
void add_watches(int fd, char *root){

	int wd;
	char *abs_dir;
	struct dirent *entry;
	DIR *dp;

	dp = opendir(root);
	if (dp == NULL){
	logString("Nie udało się otworzyć główniego folderu\n");
	exit(0);
	}

	wd = inotify_add_watch(fd, root, IN_CREATE | IN_MODIFY | IN_DELETE);
	if (wd == -1)
		logStringErr("Nie dodano folderu %s do obserwowanych \n",root);
	else{
		if ( wd == size ) {
			size *= 2;
			if(!(lista = realloc(lista, size * sizeof(foldery_t))))
				logStringErr("Nie udalo sie realokować lista");
		}
		lista[wd].sod = (char*)malloc(1024 * sizeof(char));
		lista[wd].sod = strcpy(lista[wd].sod, root);
		logString("DODANO DO LISTA[%d].SOD: %s\n",wd, lista[wd].sod);
		lista[wd].sdo = (char*)malloc(1024 * sizeof(char));
		lista[wd].sdo = zamien(root, lista[wd].sod, parametry.lista[j].sdo);
		logString("DODANO DO LISTA[%d].SDO: %s\n",wd, lista[wd].sdo);
		logString("Watching:: %s\n",root);
	}
 

	abs_dir = (char *)malloc(MAX_LEN);
	while((entry = readdir(dp))){
	  if (entry->d_name[0] != '.' && entry->d_name[1] != '.'){
		if (entry->d_type == DT_DIR){
			strcpy(abs_dir,root);

			strcat(abs_dir,entry->d_name);

			wd = inotify_add_watch(fd, abs_dir, IN_CREATE | IN_MODIFY | IN_DELETE);
			if (wd == -1)
				logString("Nie dodano podfolderu %s do obserwowanych \n",abs_dir);
			else{
				if ( wd == size ) {
					size *= 2;
					if(!(lista = realloc(lista, size * sizeof(foldery_t))))
						logStringErr("Nie udalo sie realokować lista");
				}
				lista[wd].sod = (char*)malloc(1024 * sizeof(char));
				lista[wd].sod = strcpy(lista[wd].sod, abs_dir);
				logString("DODANO DO LISTA[%d].SOD: %s\n",wd, lista[wd].sod);
				lista[wd].sdo = (char*)malloc(1024 * sizeof(char));

				lista[wd].sdo = zamien(abs_dir, lista[wd].sod, parametry.lista[j].sdo);
				int len = strlen(lista[wd].sdo);
					if(lista[wd].sdo[len-1] != '/'){
    						lista[wd].sdo[len] = '/';
    						lista[wd].sdo[len + 1] = '\0';
					}
				strcat(lista[wd].sdo,entry->d_name);
				logString("DODANO DO LISTA[%d].SDO: %s\n",wd, lista[wd].sdo);
				logString("Watching:: %s\n",abs_dir);

			}
		}
	  }
	}
   
	closedir(dp);
	//free(abs_dir);
}
