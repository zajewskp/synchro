#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>

#include "log.h"
#include "watcher.h"
#include "client.h"
#include "parametry.h"
#include "rstart.h"


//---------------mutexy-----------------------------------------

int run_watch = 0;
pthread_mutex_t lock_watch = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond_watch = PTHREAD_COND_INITIALIZER;
//--------------------------------------------------------------

//---------wątki------------------------------------------------


void *serv(void *);
void *watch(void *);


int watki(){



//------------------tworzenie watków----------------------
	pthread_t thread_serv;
	pthread_t thread_watch;


	pthread_create(&thread_serv, NULL, &serv, NULL);
		logString("poprawnie utworzono watek \n");
	pthread_create(&thread_watch, NULL, &watch, NULL);
		logString("poprawnie utworzono watek \n");

	pthread_join(thread_serv, NULL);
	pthread_join(thread_watch, NULL);



return 0;
}

void * serv(void * arg){


	logString("Uruchamianie serwera\n");
	int sockfd, accepted;
	struct sockaddr_in servaddr, cliaddr;
	socklen_t clilen;

	int yes = 1;	

	bzero(&servaddr,sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_port = htons(parametry.ports);


	sockfd = socket(AF_INET, SOCK_STREAM,0);
	setsockopt(sockfd,SOL_SOCKET,SO_REUSEADDR,&yes,sizeof(int));
	bind(sockfd, (struct sockaddr*)&servaddr, sizeof(servaddr));

	while(1){
		char msg[256];
		listen(sockfd,5);
		clilen = sizeof(cliaddr);
		accepted = accept(sockfd, (struct sockaddr*)&cliaddr, &clilen);

		bzero(msg,256);
		read(accepted,msg,255);
		logString("Otrzymano wiadomość od klienta: %s\n",msg);
		if(atoi(msg) == 1){
			pthread_cond_signal(&cond_watch);
			run_watch = 1;
			logString("ZAblokowanie wątku watch..\n");
			logString("Runwatch == %d\n", run_watch);
			write(accepted,"1",18);
		}
		else if(atoi(msg)==2){
			pthread_cond_signal(&cond_watch);
			run_watch =0;
			pthread_mutex_unlock(&lock_watch);
			logString("ODblokowanie wątku watch..\n");
			logString("Runwatch == %d\n", run_watch);
			write(accepted,"2",18);
		}

	}

}

void * watch(void * arg){

	rstart(); //to zrobią oba programy
	while(1){
		pthread_mutex_lock(&lock_watch);
		if(run_watch == 1){
			logString("czekam..\n");
                	pthread_cond_wait(&cond_watch, &lock_watch);
			break;
	   	 }
		else if(client("1") == 1) //wysyłaj
			break;
		else{
			pthread_mutex_unlock(&lock_watch);
			sleep(1);
		}
	}
	rrm(); //kasuj co zostało skasowane
	logString("Runwatch == %d\n", run_watch);
	//sleep(5);
	client("2");
	run_watch = 1;
	logString("222Runwatch == %d\n", run_watch);
  
        	pthread_mutex_lock(&lock_watch);
           	while(run_watch==1){
			logString("czekam..\n");
                	pthread_cond_wait(&cond_watch, &lock_watch);
	    	}
		pthread_mutex_unlock(&lock_watch);
	
	rsync(); //rsync resztę plików
	//sleep(5);
	client("2");
	run_watch = 1;
	logString("333Runwatch == %d\n", run_watch);

        	pthread_mutex_lock(&lock_watch);
           	while(run_watch==1){
			logString("czekam..\n");
                	pthread_cond_wait(&cond_watch, &lock_watch);
	    	}
		pthread_mutex_unlock(&lock_watch);
	
	start();
	sleep(1);
	client("2");
	watcher();


return NULL;
}
