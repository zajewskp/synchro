#ifndef WATKI_H
#define WATKI_H

#include <stdio.h>
#include <stdlib.h>

void * serv(void *);
void * watch(void *);
int watki();
#endif
